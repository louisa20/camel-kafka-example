package id.co.asyst.amala.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;


@SpringBootApplication
//@ImportResource( {"example-kafka-producer.xml", "beans.xml"} )
@ImportResource( {"example-kafka-consumer.xml", "beans.xml"} )
public class Application {

    public static void main(String[] args) throws Exception{
    	
        SpringApplication.run(Application.class, args);
    }
}